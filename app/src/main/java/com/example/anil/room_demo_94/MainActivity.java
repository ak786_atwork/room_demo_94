package com.example.anil.room_demo_94;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    AppDatabase db;
    UserDao userDao;

    private EditText mIdBox, mFirstNameBox, mLastNameBox;
    private Button mSaveBtn, mDeleteBtn,mDeleteAllBtn, mGetUserBtn;

    //static int userId = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();

         db = AppDatabase.getDatabase(getApplicationContext());
         userDao = db.userDao();

    }

    public void initialize()
    {
        mIdBox = findViewById(R.id.userId);
        mFirstNameBox = findViewById(R.id.first_name);
        mLastNameBox = findViewById(R.id.last_name);

        mSaveBtn = findViewById(R.id.save);
        mDeleteBtn = findViewById(R.id.delete);
        mDeleteAllBtn = findViewById(R.id.delete_all);
        mGetUserBtn = findViewById(R.id.get);

        mSaveBtn.setOnClickListener(this);
        mGetUserBtn.setOnClickListener(this);
        mDeleteAllBtn.setOnClickListener(this);
        mDeleteBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        String firstName = mFirstNameBox.getText().toString();
        String latName= mLastNameBox.getText().toString();
        int id = Integer.parseInt(mIdBox.getText().toString());

        switch (v.getId())
        {
            case R.id.save:
                //save
                userDao.insertAll(new User(id, firstName, latName));
                showToast("user inserted");
                break;

            case R.id.delete:
                //id = Integer.parseInt(mIdBox.getText().toString());
                userDao.delete(id);
                showToast("user deleted");
                break;

            case R.id.delete_all:
                db.clearAllTables();
                showToast("entries cleared");
                break;

            case R.id.get:
                //set first name and last name

                User user = userDao.findById(id);

                if (user != null)
                {
                    showToast("user retrieved");
                    mIdBox.setText(String.valueOf(user.getUid()));
                    mLastNameBox.setText(user.getLastName());
                    mFirstNameBox.setText(user.getFirstName());
                }

                break;

        }
    }

    public void showToast(String text)
    {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
